DateSee = [0,0]
Data = Object()
Data.Reservation = [];
Data.User = undefined;
Data.Keys = Object({"a":"","b":""})

window.onload = function(){vizualise(2021,0)}

function calendar(){

    var years = []
    D0 = new Date(2021,0,1)
    Now = new Date()
    D = new Date(2021,0,1).getTime()

    while (new Date(D).getFullYear() < Now.getFullYear()+2){
        years.push(D)
        D+=1000*60*60*24
    }
    return years
}

function vizualise(y,m) {
    
    document.getElementById('calendar').textContent = ''
    document.getElementById('month').textContent =String(new Date(1980,m)).split(' ')[1]

    years = calendar()
    let week = document.createElement('div')
    week.classList.add('week')
    let Month = document.getElementById('calendar')
    let n=0;

    years.forEach(e => {
        if (new Date(e).getFullYear()==y && new Date(e).getMonth()== m){
            
            n++
            let doc = document.createElement('span')
            doc.id="day_"+n
            doc.classList.add('day')
            if (Data.Reservation.filter(item=>item.time==e && item.owner==Data.User).length != 0
            ){doc.classList.add('dayReserve')}
            else if (include(e)){doc.classList.add('dayUnv')}
                
            doc.onclick = function(){reserve(e)}
            doc.onmousemove = function(event){info(event,e)}
            doc.onmouseleave = function(){out()}
            
            String(new Date(e)).split(' ').slice(0,4).forEach(e=>{
                doc.textContent+= e + ' '
            })
            week.appendChild(doc)

            if (String(new Date(e)).split(' ')[0]=='Sun'){
                Month.appendChild(week)
                week = document.createElement('div')
                week.classList.add('week')
            }
        }
    });
    if (week.childElementCount != 0){Month.appendChild(week); week = ''}
    out()
}

function ChangeMonth(x){
    DateSee[1]+=x
    if (DateSee[1]<0){
        DateSee[1]=11
        DateSee[0]--
        if (DateSee[0]<0){DateSee=[0,0]}
    }
    if (DateSee[1]>11){
        DateSee[1]=0
        DateSee[0]++
        if (DateSee[0]>=2)[DateSee=[1,11]]
    }
    vizualise(DateSee[0]+2021,DateSee[1])
    console.log('ChangeMonth',DateSee)
}

function reserve(info){
    
    if (Data.User != undefined){
        if ( include(info) ){
            Data.Reservation = Data.Reservation.filter(
                item => !(item.time == info && item.owner == Data.User)
            )
        } else {
            Data.Reservation.push(new Object({
                time:info,
                date: new Date(info),
                owner: Data.User
            }))
        }
        //console.log('Data.Reservation :',Data.Reservation)

        vizualise(DateSee[0]+2021,DateSee[1])
    }
}

function include(x){
    let k = 0
    Data.Reservation.forEach(e=>{if (e.time==x){k = 1}})
    return k
}

function info(event,x){

    
    document.getElementById('mouse').style.top = (event.y+5)+'px'
    document.getElementById('mouse').style.left = (event.x+5)+'px'

    //console.log('info',event,x)
    if (include(x)){
        let E = []
        Data.Reservation.forEach(e=>{if (e.time==x){E.push(e)}})

        document.getElementById('mouse').classList.remove('hidden')
        document.getElementById('mouse').textContent = 'owner : '+E[0].owner
        //console.log('INFO :',E,E[0].owner)
    }
    
    
}

function out(){
    //console.log('out')
    document.getElementById('mouse').classList.add('hidden')
}

function connection(){
    

    usr = document.getElementById('user').value
    psw = document.getElementById('password').value

    

    if (Data.Keys[usr] == psw ){
        Data.User = usr
        document.getElementById('connect').innerText = 'User : ' + Data.User
        closeUser()
    } else {
        Data.User = undefined
        document.getElementById('wrong_psw').innerText = 'Wrong Password'
        document.getElementById('password').value = ''
    }

    if (Data.User == '' || Data.User == undefined){
        Data.User = undefined
        document.getElementById('connect').innerText = "You're not Connected"
    }
    console.log('connection',usr)
    vizualise(DateSee[0]+2021,DateSee[1])
}

function SetUser(){
    document.getElementById('connection').classList.remove('hidden')
    document.getElementById('wrong_psw').innerText = ''
}

function closeUser(){
    document.getElementById('connection').classList.add('hidden')
    document.getElementById('wrong_psw').innerText = ''
}

/* 

- gitlab + data base ( Firebase https://firebase.google.com Database )
or
- Node.js + hoster

*/
